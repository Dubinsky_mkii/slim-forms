# Form Builder for a slim framework

Copy content of resources folder into your project template folder to use default templates or use them to create your own.
You can override template path in form/fields constructors.

###Example form class:

    <?php
    
    use slimsky\forms\FieldTypes\EmailField;
    use slimsky\forms\FieldTypes\TextAreaField;
    use slimsky\forms\FieldTypes\TextField;
    use slimsky\forms\Form;
    
    class TestForm extends Form {
    
        private $emailField;
        private $nameField;
        private $messageField;
    
        /**
         * @param $request
         * @param string $name
         * @param null $templatePath
         */
        public function __construct($request, $name = "contacts", $templatePath = null) {
            parent::__construct($request, $name, $templatePath);
    
            $this->emailField = new EmailField($this, "form.email.title");
            $this->add(
                $this->emailField
                    ->setId("email")
                    ->setDescription("Description")
                    ->setPlaceholder("Enter your mail")
                    ->setRequired(true));
            $this->nameField = new TextField($this, "Name");
            $this->add(
                $this->nameField
                    ->setPlaceholder("Enter Our Name")
                    ->setId("name")
                    ->setRequired(true));
            $this->messageField = new TextAreaField($this, "Message");
            $this->add(
                $this->messageField
                    ->setId("message")
                    ->setPlaceholder("Enter your message")
                    ->setRequired(true));
    
            $this->fillValues();
        }
    
        public function getEmail() {
            return $this->emailField->getValue();
        }
    
        public function getName() {
            return $this->nameField->getValue();
        }
    
        public function getMessage() {
            return $this->messageField->getValue();
        }
    
    }

###Example usage of form in a slim controller:

    $form = new TestForm($request);
    if ($form->isSubmitted() && $form->isValid()) {
        $message = $form->getEmail();
        $message .= " ".$form->getMessage();
    }
    return $this->render($response, 'page_temaplate.html.twig', Array(
        "contacts_form" => $form->getView($this->view),
    ));
    
    
###In a template (twig)

    ...
    <div>
        {{ contacts_form|raw }}
    </div>
    ...