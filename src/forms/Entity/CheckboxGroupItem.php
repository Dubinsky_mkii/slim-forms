<?php

namespace slimsky\forms\Entity;

class CheckboxGroupItem {

    public $checked = false;
    
    private $title;
    private $value;
    private $description;
    

    public function __construct($title, $value, $description) {
        $this->value = $value;
        $this->title = $title;
        $this->description = $description;
    }

    public function getValue() {
        return $this->value;
    }

    public function getTitle() {
        return $this->title;
    }
    
    public function getDescription() {
        return $this->description;
    }
}