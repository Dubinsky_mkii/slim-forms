<?php

namespace slimsky\forms\Entity;

class Country {

    private $code;
    private $title;

    public function __construct($countryCode, $title) {
        $this->code = $countryCode;
        $this->title = $title;
    }

    public function getCode() {
        return $this->code;
    }

    public function getTitle() {
        return $this->title;
    }
}