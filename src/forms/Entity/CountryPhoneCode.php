<?php

namespace slimsky\forms\Entity;

class CountryPhoneCode {

    private $countryCode;
    private $codeValue;
    private $title;

    public function __construct($countryCode, $codeValue, $title) {
        $this->countryCode = $countryCode;
        $this->codeValue = $codeValue;
        $this->title = $title;
    }

    public function getCountryCode() {
        return $this->countryCode;
    }

    public function getCodeValue() {
        return $this->codeValue;
    }

    public function getTitle() {
        return $this->title;
    }
}