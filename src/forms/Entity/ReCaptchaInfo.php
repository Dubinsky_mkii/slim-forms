<?php

namespace slimsky\forms\Entity;

class ReCaptchaInfo {

    private $siteKey;

    private $siteSecret;

    public function __construct($settings) {
        $this->siteKey = $settings['key'];
        $this->siteSecret = $settings['secret'];
    }

    public function getSitekey() {
        return $this->siteKey;
    }

    public function getSiteSecret() {
        return $this->siteSecret;
    }
}