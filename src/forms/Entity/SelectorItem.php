<?php

namespace slimsky\forms\Entity;

class SelectorItem {

    private $value;
    private $title;

    public function __construct($value, $title) {
        $this->value = $value;
        $this->title = $title;
    }

    public function getValue() {
        return $this->value;
    }

    public function getTitle() {
        return $this->title;
    }
}