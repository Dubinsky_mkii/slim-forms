<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\MaxCharsRule;
use slimsky\forms\Validation\Rules\MinCharsRule;

class CheckboxField extends FieldType {

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/checkboxfield.twig') {
        parent::__construct($owner, $title, $templatePath);
    }
    
    public function setValueFrom($value) {
        $this->value = $value[$this->id];
        return $this;
    }
}