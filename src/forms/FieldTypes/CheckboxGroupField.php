<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\MaxCharsRule;
use slimsky\forms\Validation\Rules\MinCharsRule;
use slimsky\forms\Entity\CheckboxGroupItem;
use slimsky\forms\Validation\Rules\MinChecksRule;

class CheckboxGroupField extends FieldType {
    
    public $groupItems = Array();
    
    public $selectAllEnabled = false;
    
    public $selectAllTitle = "";
    
    public $selectAllDescription = "";
    
    private $minChecksRule;
    
    
    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/checkboxgroupfield.twig') {
        parent::__construct($owner, $title, $templatePath);
        $this->minChecksRule = new MinChecksRule($this->title, 0);
        $this->addValidator($this->minChecksRule);
    }
    
    public function addItem($title, $value, $description) {
        if (array_key_exists($value, $this->groupItems)) {
            throw new \Exception("duplicate value for one checkbox group :".$value);
        }
        $this->groupItems[$value] = new CheckboxGroupItem($title, $value, $description);
        return $this;
    }
    
    public function enableSelectAll($title, $description = null) {
        $this->selectAllEnabled = true;
        $this->selectAllTitle = $title;
        $this->selectAllDescription = $description;
        return $this;
    }
    
    public function setMinimumRequired($minRequred = 1, $msg = "Please select at least one item") {
        $this->minChecksRule->setMinChecks($minRequred);
        $this->minChecksRule->message = $msg;
        return $this;
    }
    
    public function setValueFrom($value) {
        $this->value = $value[$this->id];
        if (sizeof($this->value) > 0) {
            foreach ($this->value as $value) {
                if (array_key_exists($value, $this->groupItems)) {
                    $this->groupItems[$value]->checked = true;
                }
            }
        }
        return $this;
    }
    
    protected function getRenderParams() {
        $params = parent::getRenderParams();
        $params['groupItems'] = $this->groupItems;
        $params['selectAllEnabled'] = $this->selectAllEnabled;
        $params['selectAllTitle'] = $this->selectAllTitle;
        $params['selectAllDescription'] = $this->selectAllDescription;
        return $params;
    }
    
}