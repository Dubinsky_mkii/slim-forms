<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use Slim\Views\Twig;
use slimsky\forms\Entity\Country;
use slimsky\forms\Form;
use Symfony\Component\Translation\Translator;

class CountryField extends FieldType {

    protected $countryCode;

    protected $countryCodeItems = array();

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/countryfield.twig') {
        parent::__construct($owner, $title, $templatePath);
        $this->initCountryCodeList();
        //TODO: validate valid country code
    }

    public function getCountryCode() {
        return $this->countryCode;
    }
    
    protected function getRenderParams() {
        $params = parent::getRenderParams();
        $params['countryItems'] = $this->countryCodeItems;
        return $params;
    }

    private function initCountryCodeList() {
        $this->countryCodeItems[] = new Country("AF","Afghanistan");
        $this->countryCodeItems[] = new Country("AX","Åland Islands");
        $this->countryCodeItems[] = new Country("AL","Albania");
        $this->countryCodeItems[] = new Country("DZ","Algeria");
        $this->countryCodeItems[] = new Country("AS","American Samoa");
        $this->countryCodeItems[] = new Country("AD","Andorra");
        $this->countryCodeItems[] = new Country("AO","Angola");
        $this->countryCodeItems[] = new Country("AI","Anguilla");
        $this->countryCodeItems[] = new Country("AQ","Antarctica");
        $this->countryCodeItems[] = new Country("AG","Antigua and Barbuda");
        $this->countryCodeItems[] = new Country("AR","Argentina");
        $this->countryCodeItems[] = new Country("AM","Armenia");
        $this->countryCodeItems[] = new Country("AW","Aruba");
        $this->countryCodeItems[] = new Country("AU","Australia");
        $this->countryCodeItems[] = new Country("AT","Austria");
        $this->countryCodeItems[] = new Country("AZ","Azerbaijan");
        $this->countryCodeItems[] = new Country("BS","Bahamas");
        $this->countryCodeItems[] = new Country("BH","Bahrain");
        $this->countryCodeItems[] = new Country("BD","Bangladesh");
        $this->countryCodeItems[] = new Country("BB","Barbados");
        $this->countryCodeItems[] = new Country("BY","Belarus");
        $this->countryCodeItems[] = new Country("BE","Belgium");
        $this->countryCodeItems[] = new Country("BZ","Belize");
        $this->countryCodeItems[] = new Country("BJ","Benin");
        $this->countryCodeItems[] = new Country("BM","Bermuda");
        $this->countryCodeItems[] = new Country("BT","Bhutan");
        $this->countryCodeItems[] = new Country("BO","Bolivia, Plurinational State of");
        $this->countryCodeItems[] = new Country("BQ","Bonaire, Sint Eustatius and Saba");
        $this->countryCodeItems[] = new Country("BA","Bosnia and Herzegovina");
        $this->countryCodeItems[] = new Country("BW","Botswana");
        $this->countryCodeItems[] = new Country("BV","Bouvet Island");
        $this->countryCodeItems[] = new Country("BR","Brazil");
        $this->countryCodeItems[] = new Country("IO","British Indian Ocean Territory");
        $this->countryCodeItems[] = new Country("BN","Brunei Darussalam");
        $this->countryCodeItems[] = new Country("BG","Bulgaria");
        $this->countryCodeItems[] = new Country("BF","Burkina Faso");
        $this->countryCodeItems[] = new Country("BI","Burundi");
        $this->countryCodeItems[] = new Country("KH","Cambodia");
        $this->countryCodeItems[] = new Country("CM","Cameroon");
        $this->countryCodeItems[] = new Country("CA","Canada");
        $this->countryCodeItems[] = new Country("CV","Cape Verde");
        $this->countryCodeItems[] = new Country("KY","Cayman Islands");
        $this->countryCodeItems[] = new Country("CF","Central African Republic");
        $this->countryCodeItems[] = new Country("TD","Chad");
        $this->countryCodeItems[] = new Country("CL","Chile");
        $this->countryCodeItems[] = new Country("CN","China");
        $this->countryCodeItems[] = new Country("CX","Christmas Island");
        $this->countryCodeItems[] = new Country("CC","Cocos (Keeling) Islands");
        $this->countryCodeItems[] = new Country("CO","Colombia");
        $this->countryCodeItems[] = new Country("KM","Comoros");
        $this->countryCodeItems[] = new Country("CG","Congo");
        $this->countryCodeItems[] = new Country("CD","Congo, the Democratic Republic of the");
        $this->countryCodeItems[] = new Country("CK","Cook Islands");
        $this->countryCodeItems[] = new Country("CR","Costa Rica");
        $this->countryCodeItems[] = new Country("CI","Côte d'Ivoire");
        $this->countryCodeItems[] = new Country("HR","Croatia");
        $this->countryCodeItems[] = new Country("CU","Cuba");
        $this->countryCodeItems[] = new Country("CW","Curaçao");
        $this->countryCodeItems[] = new Country("CY","Cyprus");
        $this->countryCodeItems[] = new Country("CZ","Czech Republic");
        $this->countryCodeItems[] = new Country("DK","Denmark");
        $this->countryCodeItems[] = new Country("DJ","Djibouti");
        $this->countryCodeItems[] = new Country("DM","Dominica");
        $this->countryCodeItems[] = new Country("DO","Dominican Republic");
        $this->countryCodeItems[] = new Country("EC","Ecuador");
        $this->countryCodeItems[] = new Country("EG","Egypt");
        $this->countryCodeItems[] = new Country("SV","El Salvador");
        $this->countryCodeItems[] = new Country("GQ","Equatorial Guinea");
        $this->countryCodeItems[] = new Country("ER","Eritrea");
        $this->countryCodeItems[] = new Country("EE","Estonia");
        $this->countryCodeItems[] = new Country("ET","Ethiopia");
        $this->countryCodeItems[] = new Country("FK","Falkland Islands (Malvinas)");
        $this->countryCodeItems[] = new Country("FO","Faroe Islands");
        $this->countryCodeItems[] = new Country("FJ","Fiji");
        $this->countryCodeItems[] = new Country("FI","Finland");
        $this->countryCodeItems[] = new Country("FR","France");
        $this->countryCodeItems[] = new Country("GF","French Guiana");
        $this->countryCodeItems[] = new Country("PF","French Polynesia");
        $this->countryCodeItems[] = new Country("TF","French Southern Territories");
        $this->countryCodeItems[] = new Country("GA","Gabon");
        $this->countryCodeItems[] = new Country("GM","Gambia");
        $this->countryCodeItems[] = new Country("GE","Georgia");
        $this->countryCodeItems[] = new Country("DE","Germany");
        $this->countryCodeItems[] = new Country("GH","Ghana");
        $this->countryCodeItems[] = new Country("GI","Gibraltar");
        $this->countryCodeItems[] = new Country("GR","Greece");
        $this->countryCodeItems[] = new Country("GL","Greenland");
        $this->countryCodeItems[] = new Country("GD","Grenada");
        $this->countryCodeItems[] = new Country("GP","Guadeloupe");
        $this->countryCodeItems[] = new Country("GU","Guam");
        $this->countryCodeItems[] = new Country("GT","Guatemala");
        $this->countryCodeItems[] = new Country("GG","Guernsey");
        $this->countryCodeItems[] = new Country("GN","Guinea");
        $this->countryCodeItems[] = new Country("GW","Guinea-Bissau");
        $this->countryCodeItems[] = new Country("GY","Guyana");
        $this->countryCodeItems[] = new Country("HT","Haiti");
        $this->countryCodeItems[] = new Country("HM","Heard Island and McDonald Islands");
        $this->countryCodeItems[] = new Country("VA","Holy See (Vatican City State)");
        $this->countryCodeItems[] = new Country("HN","Honduras");
        $this->countryCodeItems[] = new Country("HK","Hong Kong");
        $this->countryCodeItems[] = new Country("HU","Hungary");
        $this->countryCodeItems[] = new Country("IS","Iceland");
        $this->countryCodeItems[] = new Country("IN","India");
        $this->countryCodeItems[] = new Country("ID","Indonesia");
        $this->countryCodeItems[] = new Country("IR","Iran, Islamic Republic of");
        $this->countryCodeItems[] = new Country("IQ","Iraq");
        $this->countryCodeItems[] = new Country("IE","Ireland");
        $this->countryCodeItems[] = new Country("IM","Isle of Man");
        $this->countryCodeItems[] = new Country("IL","Israel");
        $this->countryCodeItems[] = new Country("IT","Italy");
        $this->countryCodeItems[] = new Country("JM","Jamaica");
        $this->countryCodeItems[] = new Country("JP","Japan");
        $this->countryCodeItems[] = new Country("JE","Jersey");
        $this->countryCodeItems[] = new Country("JO","Jordan");
        $this->countryCodeItems[] = new Country("KZ","Kazakhstan");
        $this->countryCodeItems[] = new Country("KE","Kenya");
        $this->countryCodeItems[] = new Country("KI","Kiribati");
        $this->countryCodeItems[] = new Country("KP","Korea, Democratic People's Republic of");
        $this->countryCodeItems[] = new Country("KR","Korea, Republic of");
        $this->countryCodeItems[] = new Country("KW","Kuwait");
        $this->countryCodeItems[] = new Country("KG","Kyrgyzstan");
        $this->countryCodeItems[] = new Country("LA","Lao People's Democratic Republic");
        $this->countryCodeItems[] = new Country("LV","Latvia");
        $this->countryCodeItems[] = new Country("LB","Lebanon");
        $this->countryCodeItems[] = new Country("LS","Lesotho");
        $this->countryCodeItems[] = new Country("LR","Liberia");
        $this->countryCodeItems[] = new Country("LY","Libya");
        $this->countryCodeItems[] = new Country("LI","Liechtenstein");
        $this->countryCodeItems[] = new Country("LT","Lithuania");
        $this->countryCodeItems[] = new Country("LU","Luxembourg");
        $this->countryCodeItems[] = new Country("MO","Macao");
        $this->countryCodeItems[] = new Country("MK","Macedonia, the former Yugoslav Republic of");
        $this->countryCodeItems[] = new Country("MG","Madagascar");
        $this->countryCodeItems[] = new Country("MW","Malawi");
        $this->countryCodeItems[] = new Country("MY","Malaysia");
        $this->countryCodeItems[] = new Country("MV","Maldives");
        $this->countryCodeItems[] = new Country("ML","Mali");
        $this->countryCodeItems[] = new Country("MT","Malta");
        $this->countryCodeItems[] = new Country("MH","Marshall Islands");
        $this->countryCodeItems[] = new Country("MQ","Martinique");
        $this->countryCodeItems[] = new Country("MR","Mauritania");
        $this->countryCodeItems[] = new Country("MU","Mauritius");
        $this->countryCodeItems[] = new Country("YT","Mayotte");
        $this->countryCodeItems[] = new Country("MX","Mexico");
        $this->countryCodeItems[] = new Country("FM","Micronesia, Federated States of");
        $this->countryCodeItems[] = new Country("MD","Moldova, Republic of");
        $this->countryCodeItems[] = new Country("MC","Monaco");
        $this->countryCodeItems[] = new Country("MN","Mongolia");
        $this->countryCodeItems[] = new Country("ME","Montenegro");
        $this->countryCodeItems[] = new Country("MS","Montserrat");
        $this->countryCodeItems[] = new Country("MA","Morocco");
        $this->countryCodeItems[] = new Country("MZ","Mozambique");
        $this->countryCodeItems[] = new Country("MM","Myanmar");
        $this->countryCodeItems[] = new Country("NA","Namibia");
        $this->countryCodeItems[] = new Country("NR","Nauru");
        $this->countryCodeItems[] = new Country("NP","Nepal");
        $this->countryCodeItems[] = new Country("NL","Netherlands");
        $this->countryCodeItems[] = new Country("NC","New Caledonia");
        $this->countryCodeItems[] = new Country("NZ","New Zealand");
        $this->countryCodeItems[] = new Country("NI","Nicaragua");
        $this->countryCodeItems[] = new Country("NE","Niger");
        $this->countryCodeItems[] = new Country("NG","Nigeria");
        $this->countryCodeItems[] = new Country("NU","Niue");
        $this->countryCodeItems[] = new Country("NF","Norfolk Island");
        $this->countryCodeItems[] = new Country("MP","Northern Mariana Islands");
        $this->countryCodeItems[] = new Country("NO","Norway");
        $this->countryCodeItems[] = new Country("OM","Oman");
        $this->countryCodeItems[] = new Country("PK","Pakistan");
        $this->countryCodeItems[] = new Country("PW","Palau");
        $this->countryCodeItems[] = new Country("PS","Palestinian Territory, Occupied");
        $this->countryCodeItems[] = new Country("PA","Panama");
        $this->countryCodeItems[] = new Country("PG","Papua New Guinea");
        $this->countryCodeItems[] = new Country("PY","Paraguay");
        $this->countryCodeItems[] = new Country("PE","Peru");
        $this->countryCodeItems[] = new Country("PH","Philippines");
        $this->countryCodeItems[] = new Country("PN","Pitcairn");
        $this->countryCodeItems[] = new Country("PL","Poland");
        $this->countryCodeItems[] = new Country("PT","Portugal");
        $this->countryCodeItems[] = new Country("PR","Puerto Rico");
        $this->countryCodeItems[] = new Country("QA","Qatar");
        $this->countryCodeItems[] = new Country("RE","Réunion");
        $this->countryCodeItems[] = new Country("RO","Romania");
        $this->countryCodeItems[] = new Country("RU","Russian Federation");
        $this->countryCodeItems[] = new Country("RW","Rwanda");
        $this->countryCodeItems[] = new Country("BL","Saint Barthélemy");
        $this->countryCodeItems[] = new Country("SH","Saint Helena, Ascension and Tristan da Cunha");
        $this->countryCodeItems[] = new Country("KN","Saint Kitts and Nevis");
        $this->countryCodeItems[] = new Country("LC","Saint Lucia");
        $this->countryCodeItems[] = new Country("MF","Saint Martin (French part)");
        $this->countryCodeItems[] = new Country("PM","Saint Pierre and Miquelon");
        $this->countryCodeItems[] = new Country("VC","Saint Vincent and the Grenadines");
        $this->countryCodeItems[] = new Country("WS","Samoa");
        $this->countryCodeItems[] = new Country("SM","San Marino");
        $this->countryCodeItems[] = new Country("ST","Sao Tome and Principe");
        $this->countryCodeItems[] = new Country("SA","Saudi Arabia");
        $this->countryCodeItems[] = new Country("SN","Senegal");
        $this->countryCodeItems[] = new Country("RS","Serbia");
        $this->countryCodeItems[] = new Country("SC","Seychelles");
        $this->countryCodeItems[] = new Country("SL","Sierra Leone");
        $this->countryCodeItems[] = new Country("SG","Singapore");
        $this->countryCodeItems[] = new Country("SX","Sint Maarten (Dutch part)");
        $this->countryCodeItems[] = new Country("SK","Slovakia");
        $this->countryCodeItems[] = new Country("SI","Slovenia");
        $this->countryCodeItems[] = new Country("SB","Solomon Islands");
        $this->countryCodeItems[] = new Country("SO","Somalia");
        $this->countryCodeItems[] = new Country("ZA","South Africa");
        $this->countryCodeItems[] = new Country("GS","South Georgia and the South Sandwich Islands");
        $this->countryCodeItems[] = new Country("SS","South Sudan");
        $this->countryCodeItems[] = new Country("ES","Spain");
        $this->countryCodeItems[] = new Country("LK","Sri Lanka");
        $this->countryCodeItems[] = new Country("SD","Sudan");
        $this->countryCodeItems[] = new Country("SR","Suriname");
        $this->countryCodeItems[] = new Country("SJ","Svalbard and Jan Mayen");
        $this->countryCodeItems[] = new Country("SZ","Swaziland");
        $this->countryCodeItems[] = new Country("SE","Sweden");
        $this->countryCodeItems[] = new Country("CH","Switzerland");
        $this->countryCodeItems[] = new Country("SY","Syrian Arab Republic");
        $this->countryCodeItems[] = new Country("TW","Taiwan, Province of China");
        $this->countryCodeItems[] = new Country("TJ","Tajikistan");
        $this->countryCodeItems[] = new Country("TZ","Tanzania, United Republic of");
        $this->countryCodeItems[] = new Country("TH","Thailand");
        $this->countryCodeItems[] = new Country("TL","Timor-Leste");
        $this->countryCodeItems[] = new Country("TG","Togo");
        $this->countryCodeItems[] = new Country("TK","Tokelau");
        $this->countryCodeItems[] = new Country("TO","Tonga");
        $this->countryCodeItems[] = new Country("TT","Trinidad and Tobago");
        $this->countryCodeItems[] = new Country("TN","Tunisia");
        $this->countryCodeItems[] = new Country("TR","Turkey");
        $this->countryCodeItems[] = new Country("TM","Turkmenistan");
        $this->countryCodeItems[] = new Country("TC","Turks and Caicos Islands");
        $this->countryCodeItems[] = new Country("TV","Tuvalu");
        $this->countryCodeItems[] = new Country("UG","Uganda");
        $this->countryCodeItems[] = new Country("UA","Ukraine");
        $this->countryCodeItems[] = new Country("AE","United Arab Emirates");
        $this->countryCodeItems[] = new Country("GB","United Kingdom");
        $this->countryCodeItems[] = new Country("US","United States");
        $this->countryCodeItems[] = new Country("UM","United States Minor Outlying Islands");
        $this->countryCodeItems[] = new Country("UY","Uruguay");
        $this->countryCodeItems[] = new Country("UZ","Uzbekistan");
        $this->countryCodeItems[] = new Country("VU","Vanuatu");
        $this->countryCodeItems[] = new Country("VE","Venezuela, Bolivarian Republic of");
        $this->countryCodeItems[] = new Country("VN","Viet Nam");
        $this->countryCodeItems[] = new Country("VG","Virgin Islands, British");
        $this->countryCodeItems[] = new Country("VI","Virgin Islands, U.S.");
        $this->countryCodeItems[] = new Country("WF","Wallis and Futuna");
        $this->countryCodeItems[] = new Country("EH","Western Sahara");
        $this->countryCodeItems[] = new Country("YE","Yemen");
        $this->countryCodeItems[] = new Country("ZM","Zambia");
        $this->countryCodeItems[] = new Country("ZW","Zimbabwe");
    }
}