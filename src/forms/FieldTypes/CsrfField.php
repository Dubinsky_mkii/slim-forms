<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\CsrfRule;

class CsrfField extends FieldType {

    private $fieldName;

    private $csrfRule;

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/csrf.twig') {
        parent::__construct($owner, $title, $templatePath);
        $this->fieldName = 'sha-2-tkn_'.$this->owner->getId()."_".$this->id;
        $this->csrfRule = new CsrfRule('csrf', $this);
        $this->addValidator($this->csrfRule);
        if (!isset($_SESSION[$this->fieldName])) {
            $this->value = md5(uniqid(rand(), TRUE));
            $_SESSION[$this->fieldName] = $this->value;
        } else {
            $this->value = $_SESSION[$this->fieldName];
        }
    }

    public function getCsrfRule() {
        return $this->csrfRule;
    }

    public function getFieldName() {
        return $this->fieldName;
    }

}