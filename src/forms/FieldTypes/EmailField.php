<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use slimsky\forms\Form;
use Respect\Validation\Validator as v;
use slimsky\forms\Validation\Rules\EmailRule;

class EmailField extends FieldType {

    private $emailValidator;

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/emailfield.twig') {
        parent::__construct($owner, $title, $templatePath);
        $this->emailValidator = new EmailRule($title);
        $this->addValidator($this->emailValidator);
    }

    public function getEMailRule() {
        return $this->emailValidator;
    }
}