<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:44
 */

namespace slimsky\forms\FieldTypes;

use Slim\Views\Twig;
use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\AbstractRule;
use slimsky\forms\Validation\Rules\NotBlankRule;
use Symfony\Component\Translation\Translator;


class FieldType {

    protected $id;

    protected $title;

    protected $description;

    protected $placeholder;

    protected $value;

    /** @var AbstractRule[] */
    protected $validators = array();

    protected $owner;

    protected $required;

    protected $requiredErrorMessage;

    protected $templatePath;

    protected $errors = array();

    protected $notBlankRule;


    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath) {
        $this->owner = $owner;
        $this->title = $title;
        $this->templatePath = $templatePath;
        $this->notBlankRule = new NotBlankRule($this->title);
        $this->addValidator($this->notBlankRule);
        $this->setRequired(false);
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getId() {
        return $this->id;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function setPlaceholder($placeholder) {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function getValue() {
        return $this->value;
    }

    public function setValueFrom($value) {
        $this->value = $value[$this->id];
        return $this;
    }

    public function setValue($value) {
        $this->value = $value;
        return $this;
    }

    /**
     * @param $validator AbstractRule
     * @return $this
     */
    public function addValidator($validator) {
        $this->validators[] = $validator;
        return $this;
    }

    public function setRequired($required, $requiredErrorMessage = null) {
        $this->required = $required;
        $this->notBlankRule->setEnabled($required);
        $this->notBlankRule->message = $requiredErrorMessage;
        return $this;
    }

    public function getNotBlankRule() {
        return $this->notBlankRule;
    }

    public function isValid() {
        foreach($this->validators as $validatorRule) {
            if (!$validatorRule->validate($this->getValue())) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $error string - error message
     */
    public function addError($error, $translator = null) {
        if ($error !== null) {
            if (!$this->haveErrorMsg($error)) {
                $this->errors[] = $error;
            }
        }
    }

    public function haveErrorMsg($error) {
        if (!$this->errors) {
            return false;
        }
        foreach ($this->errors as $errorMsg) {
            if ($error == $errorMsg) {
                return true;
            }
        }
    }

    /**
     * @param $translator Translator
     * @return array
     */
    public function getErrors($translator = null) {
        $this->errors = Array();
        foreach($this->validators as $validatorRule) {
            $this->addError($validatorRule->getErrors($this->getValue(), $translator));
        }
        return $this->errors;
    }

    /**
     * @param $view Twig
     * @param $translator Translator
     * @param bool $validate
     * @return string
     */
    public function getView($view, $translator, $validate = false) {
        if ($validate) {
            $this->getErrors($translator);
        }
        $params = $this->getRenderParams();
        $params['validate'] = $validate;
        return $view->fetch($this->templatePath, $params);
    }
    
    protected function getRenderParams() {
        return Array (
            "title" => $this->title,
            "id" => $this->id,
            "placeholder" => $this->placeholder,
            "description" => $this->description,
            "value" => $this->value,
            "ownerId" => $this->owner->getId(),
            "errors" => $this->errors,
            "required" => $this->required,
            "fieldEntity" => $this
        );
    }
}