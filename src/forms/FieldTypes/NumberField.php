<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\MaxRule;
use slimsky\forms\Validation\Rules\MinRule;

class NumberField extends FieldType {

    private $minRule;

    private $maxRule;

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/numberfield.twig') {
        parent::__construct($owner, $title, $templatePath);
        $this->minRule = new MinRule($this->title, 0);
        $this->maxRule = new MaxRule($this->title, PHP_INT_MAX);
        $this->addValidator($this->minRule);
        $this->addValidator($this->maxRule);
    }

    public function setMinValue($value, $errorMessage = null) {
        $this->minRule->setMinValue($value);
        if ($errorMessage) {
            $this->minRule->message = $errorMessage;
        }
        return $this;
    }

    public function setMaxValue($value, $errorMessage = null) {
        $this->maxRule->setMaxValue($value);
        if ($errorMessage) {
            $this->maxRule->message = $errorMessage;
        }
        return $this;
    }

    public function getMinRule() {
        return $this->minRule;
    }

    public function getMaxRule() {
        return $this->maxRule;
    }
}