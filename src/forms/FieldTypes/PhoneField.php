<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use Slim\Views\Twig;
use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\NotBlankRule;
use slimsky\forms\Validation\Rules\PhoneRule;
use Symfony\Component\Translation\Translator;
use slimsky\forms\Entity\CountryPhoneCode;

class PhoneField extends FieldType {

    protected $countryPhoneCode;

    protected $phoneNumber;
    
    protected $countryCodeItems = array();

    protected $phoneRule;

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/phonefield.twig') {
        parent::__construct($owner, $title, $templatePath);
        $this->initCountryCodeList();
        $this->phoneRule = new PhoneRule($this->title, $this);
        $this->addValidator($this->phoneRule);
    }

    public function setRequired($required, $requiredErrorMessage = null) {
        parent::setRequired($required, $requiredErrorMessage);
        $this->phoneRule->setEnabled($required);
    }

    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    public function getPhoneCode() {
        return $this->countryPhoneCode;
    }

    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    public function setPhoneCode($countryPhoneCode) {
        $this->countryPhoneCode = $countryPhoneCode;
    }

    public function setValueFrom($value) {
        $this->phoneNumber = $value[$this->id.'phone'];
        $this->countryPhoneCode = $value[$this->id.'phoneCode'];
    }

    public function getPhoneRule() {
        return $this->phoneRule;
    }

    protected function getRenderParams() {
        $params = parent::getRenderParams();
        $params['countryCodeItems'] = $this->countryCodeItems;
        $params['countryPhoneCode'] = $this->countryPhoneCode;
        $params['phoneNumber'] = $this->phoneNumber;
        return $params;
    }

    private function initCountryCodeList() {
        /*
        <!-- Countries often selected by users can be moved to the top of the list. -->
        <!-- Countries known to be subject to general US embargo are commented out by default. -->
        <!-- The data-countryCode attribute is populated with ISO country code, and value is int'l calling code. -->
         */
        $this->countryCodeItems[] = new CountryPhoneCode( "US" ,1, "USA (+1)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GB" ,44, "UK (+44)");
        $this->countryCodeItems[] = new CountryPhoneCode( "DZ" ,213, "Algeria (+213)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AD" ,376, "Andorra (+376)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AO" ,244, "Angola (+244)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AI" ,1264, "Anguilla (+1264)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AG" ,1268, "Antigua &amp; Barbuda (+1268)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AR" ,54, "Argentina (+54)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AM" ,374, "Armenia (+374)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AW" ,297, "Aruba (+297)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AU" ,61, "Australia (+61)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AT" ,43, "Austria (+43)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AZ" ,994, "Azerbaijan (+994)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BS" ,1242, "Bahamas (+1242)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BH" ,973, "Bahrain (+973)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BD" ,880, "Bangladesh (+880)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BB" ,1246, "Barbados (+1246)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BY" ,375, "Belarus (+375)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BE" ,32, "Belgium (+32)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BZ" ,501, "Belize (+501)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BJ" ,229, "Benin (+229)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BM" ,1441, "Bermuda (+1441)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BT" ,975, "Bhutan (+975)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BO" ,591, "Bolivia (+591)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BA" ,387, "Bosnia Herzegovina (+387)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BW" ,267, "Botswana (+267)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BR" ,55, "Brazil (+55)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BN" ,673, "Brunei (+673)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BG" ,359, "Bulgaria (+359)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BF" ,226, "Burkina Faso (+226)");
        $this->countryCodeItems[] = new CountryPhoneCode( "BI" ,257, "Burundi (+257)");
        $this->countryCodeItems[] = new CountryPhoneCode( "KH" ,855, "Cambodia (+855)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CM" ,237, "Cameroon (+237)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CA" ,1, "Canada (+1)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CV" ,238, "Cape Verde Islands (+238)");
        $this->countryCodeItems[] = new CountryPhoneCode( "KY" ,1345, "Cayman Islands (+1345)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CF" ,236, "Central African Republic (+236)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CL" ,56, "Chile (+56)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CN" ,86, "China (+86)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CO" ,57, "Colombia (+57)");
        $this->countryCodeItems[] = new CountryPhoneCode( "KM" ,269, "Comoros (+269)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CG" ,242, "Congo (+242)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CK" ,682, "Cook Islands (+682)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CR" ,506, "Costa Rica (+506)");
        $this->countryCodeItems[] = new CountryPhoneCode( "HR" ,385, "Croatia (+385)");
        //<!-- <option data-countryCode="CU" value="53", "Cuba (+53)"); -->
        $this->countryCodeItems[] = new CountryPhoneCode( "CY", 90, "Cyprus - North (+90)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CY", 357, "Cyprus - South (+357)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CZ", 420, "Czech Republic (+420)");
        $this->countryCodeItems[] = new CountryPhoneCode( "DK", 45, "Denmark (+45)");
        $this->countryCodeItems[] = new CountryPhoneCode( "DJ", 253, "Djibouti (+253)");
        $this->countryCodeItems[] = new CountryPhoneCode( "DM", 1809, "Dominica (+1809)");
        $this->countryCodeItems[] = new CountryPhoneCode( "DO", 1809, "Dominican Republic (+1809)");
        $this->countryCodeItems[] = new CountryPhoneCode( "EC", 593, "Ecuador (+593)");
        $this->countryCodeItems[] = new CountryPhoneCode( "EG", 20, "Egypt (+20)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SV", 503, "El Salvador (+503)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GQ", 240, "Equatorial Guinea (+240)");
        $this->countryCodeItems[] = new CountryPhoneCode( "ER", 291, "Eritrea (+291)");
        $this->countryCodeItems[] = new CountryPhoneCode( "EE", 372, "Estonia (+372)");
        $this->countryCodeItems[] = new CountryPhoneCode( "ET", 251, "Ethiopia (+251)");
        $this->countryCodeItems[] = new CountryPhoneCode( "FK", 500, "Falkland Islands (+500)");
        $this->countryCodeItems[] = new CountryPhoneCode( "FO", 298, "Faroe Islands (+298)");
        $this->countryCodeItems[] = new CountryPhoneCode( "FJ", 679, "Fiji (+679)");
        $this->countryCodeItems[] = new CountryPhoneCode( "FI", 358, "Finland (+358)");
        $this->countryCodeItems[] = new CountryPhoneCode( "FR", 33, "France (+33)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GF", 594, "French Guiana (+594)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PF", 689, "French Polynesia (+689)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GA", 241, "Gabon (+241)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GM", 220, "Gambia (+220)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GE", 7880, "Georgia (+7880)");
        $this->countryCodeItems[] = new CountryPhoneCode( "DE", 49, "Germany (+49)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GH", 233, "Ghana (+233)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GI", 350, "Gibraltar (+350)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GR", 30, "Greece (+30)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GL", 299, "Greenland (+299)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GD", 1473, "Grenada (+1473)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GP", 590, "Guadeloupe (+590)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GU", 671, "Guam (+671)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GT", 502, "Guatemala (+502)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GN", 224, "Guinea (+224)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GW", 245, "Guinea - Bissau (+245)");
        $this->countryCodeItems[] = new CountryPhoneCode( "GY", 592, "Guyana (+592)");
        $this->countryCodeItems[] = new CountryPhoneCode( "HT", 509, "Haiti (+509)");
        $this->countryCodeItems[] = new CountryPhoneCode( "HN", 504, "Honduras (+504)");
        $this->countryCodeItems[] = new CountryPhoneCode( "HK", 852, "Hong Kong (+852)");
        $this->countryCodeItems[] = new CountryPhoneCode( "HU", 36, "Hungary (+36)");
        $this->countryCodeItems[] = new CountryPhoneCode( "IS", 354, "Iceland (+354)");
        $this->countryCodeItems[] = new CountryPhoneCode( "IN", 91, "India (+91)");
        $this->countryCodeItems[] = new CountryPhoneCode( "ID", 62, "Indonesia (+62)");
        $this->countryCodeItems[] = new CountryPhoneCode( "IQ", 964, "Iraq (+964)");
        //<!-- <option data-countryCode="IR" value="98", "Iran (+98)"); -->
        $this->countryCodeItems[] = new CountryPhoneCode( "IE", 353, "Ireland (+353)");
        $this->countryCodeItems[] = new CountryPhoneCode( "IL", 972, "Israel (+972)");
        $this->countryCodeItems[] = new CountryPhoneCode( "IT", 39, "Italy (+39)");
        $this->countryCodeItems[] = new CountryPhoneCode( "JM", 1876, "Jamaica (+1876)");
        $this->countryCodeItems[] = new CountryPhoneCode( "JP", 81, "Japan (+81)");
        $this->countryCodeItems[] = new CountryPhoneCode( "JO", 962, "Jordan (+962)");
        $this->countryCodeItems[] = new CountryPhoneCode( "KZ", 7, "Kazakhstan (+7)");
        $this->countryCodeItems[] = new CountryPhoneCode( "KE", 254, "Kenya (+254)");
        $this->countryCodeItems[] = new CountryPhoneCode( "KI", 686, "Kiribati (+686)");
        //<!-- <option data-countryCode="KP" value="850", "Korea - North (+850)"); -->
        $this->countryCodeItems[] = new CountryPhoneCode( "KR", 82, "Korea - South (+82)");
        $this->countryCodeItems[] = new CountryPhoneCode( "KW", 965, "Kuwait (+965)");
        $this->countryCodeItems[] = new CountryPhoneCode( "KG", 996, "Kyrgyzstan (+996)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LA", 856, "Laos (+856)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LV", 371, "Latvia (+371)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LB", 961, "Lebanon (+961)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LS", 266, "Lesotho (+266)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LR", 231, "Liberia (+231)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LY", 218, "Libya (+218)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LI", 417, "Liechtenstein (+417)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LT", 370, "Lithuania (+370)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LU", 352, "Luxembourg (+352)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MO", 853, "Macao (+853)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MK", 389, "Macedonia (+389)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MG", 261, "Madagascar (+261)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MW", 265, "Malawi (+265)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MY", 60, "Malaysia (+60)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MV", 960, "Maldives (+960)");
        $this->countryCodeItems[] = new CountryPhoneCode( "ML", 223, "Mali (+223)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MT", 356, "Malta (+356)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MH", 692, "Marshall Islands (+692)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MQ", 596, "Martinique (+596)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MR", 222, "Mauritania (+222)");
        $this->countryCodeItems[] = new CountryPhoneCode( "YT", 269, "Mayotte (+269)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MX", 52, "Mexico (+52)");
        $this->countryCodeItems[] = new CountryPhoneCode( "FM", 691, "Micronesia (+691)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MD", 373, "Moldova (+373)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MC", 377, "Monaco (+377)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MN", 976, "Mongolia (+976)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MS", 1664, "Montserrat (+1664)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MA", 212, "Morocco (+212)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MZ", 258, "Mozambique (+258)");
        $this->countryCodeItems[] = new CountryPhoneCode( "MN", 95, "Myanmar (+95)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NA", 264, "Namibia (+264)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NR", 674, "Nauru (+674)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NP", 977, "Nepal (+977)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NL", 31, "Netherlands (+31)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NC", 687, "New Caledonia (+687)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NZ", 64, "New Zealand (+64)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NI", 505, "Nicaragua (+505)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NE", 227, "Niger (+227)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NG", 234, "Nigeria (+234)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NU", 683, "Niue (+683)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NF", 672, "Norfolk Islands (+672)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NP", 670, "Northern Marianas (+670)");
        $this->countryCodeItems[] = new CountryPhoneCode( "NO", 47, "Norway (+47)");
        $this->countryCodeItems[] = new CountryPhoneCode( "OM", 968, "Oman (+968)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PK", 92, "Pakistan (+92)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PW", 680, "Palau (+680)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PA", 507, "Panama (+507)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PG", 675, "Papua New Guinea (+675)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PY", 595, "Paraguay (+595)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PE", 51, "Peru (+51)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PH", 63, "Philippines (+63)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PL", 48, "Poland (+48)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PT", 351, "Portugal (+351)");
        $this->countryCodeItems[] = new CountryPhoneCode( "PR", 1787, "Puerto Rico (+1787)");
        $this->countryCodeItems[] = new CountryPhoneCode( "QA", 974, "Qatar (+974)");
        $this->countryCodeItems[] = new CountryPhoneCode( "RE", 262, "Reunion (+262)");
        $this->countryCodeItems[] = new CountryPhoneCode( "RO", 40, "Romania (+40)");
        $this->countryCodeItems[] = new CountryPhoneCode( "RU", 7, "Russia (+7)");
        $this->countryCodeItems[] = new CountryPhoneCode( "RW", 250, "Rwanda (+250)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SM", 378, "San Marino (+378)");
        $this->countryCodeItems[] = new CountryPhoneCode( "ST", 239, "Sao Tome &amp; Principe (+239)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SA", 966, "Saudi Arabia (+966)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SN", 221, "Senegal (+221)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CS", 381, "Serbia (+381)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SC", 248, "Seychelles (+248)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SL", 232, "Sierra Leone (+232)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SG", 65, "Singapore (+65)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SK", 421, "Slovak Republic (+421)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SI", 386, "Slovenia (+386)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SB", 677, "Solomon Islands (+677)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SO", 252, "Somalia (+252)");
        $this->countryCodeItems[] = new CountryPhoneCode( "ZA", 27, "South Africa (+27)");
        $this->countryCodeItems[] = new CountryPhoneCode( "ES", 34, "Spain (+34)");
        $this->countryCodeItems[] = new CountryPhoneCode( "LK", 94, "Sri Lanka (+94)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SH", 290, "St. Helena (+290)");
        $this->countryCodeItems[] = new CountryPhoneCode( "KN", 1869, "St. Kitts (+1869)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SC", 1758, "St. Lucia (+1758)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SR", 597, "Suriname (+597)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SD", 249, "Sudan (+249)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SZ", 268, "Swaziland (+268)");
        $this->countryCodeItems[] = new CountryPhoneCode( "SE", 46, "Sweden (+46)");
        $this->countryCodeItems[] = new CountryPhoneCode( "CH", 41, "Switzerland (+41)");
        //<!-- <option data-countryCode="SY" value="963", "Syria (+963)"); -->
        $this->countryCodeItems[] = new CountryPhoneCode( "TW", 886, "Taiwan (+886)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TJ", 992, "Tajikistan (+992)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TH", 66, "Thailand (+66)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TG", 228, "Togo (+228)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TO", 676, "Tonga (+676)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TT", 1868, "Trinidad &amp; Tobago (+1868)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TN", 216, "Tunisia (+216)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TR", 90, "Turkey (+90)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TM", 993, "Turkmenistan (+993)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TC", 1649, "Turks &amp; Caicos Islands (+1649)");
        $this->countryCodeItems[] = new CountryPhoneCode( "TV", 688, "Tuvalu (+688)");
        $this->countryCodeItems[] = new CountryPhoneCode( "UG", 256, "Uganda (+256)");
        $this->countryCodeItems[] = new CountryPhoneCode( "UA", 380, "Ukraine (+380)");
        $this->countryCodeItems[] = new CountryPhoneCode( "AE", 971, "United Arab Emirates (+971)");
        $this->countryCodeItems[] = new CountryPhoneCode( "UY", 598, "Uruguay (+598)");
        $this->countryCodeItems[] = new CountryPhoneCode( "UZ", 998, "Uzbekistan (+998)");
        $this->countryCodeItems[] = new CountryPhoneCode( "VU", 678, "Vanuatu (+678)");
        $this->countryCodeItems[] = new CountryPhoneCode( "VA", 379, "Vatican City (+379)");
        $this->countryCodeItems[] = new CountryPhoneCode( "VE", 58, "Venezuela (+58)");
        $this->countryCodeItems[] = new CountryPhoneCode( "VN", 84, "Vietnam (+84)");
        $this->countryCodeItems[] = new CountryPhoneCode( "VG", 1, "Virgin Islands - British (+1)");
        $this->countryCodeItems[] = new CountryPhoneCode( "VI", 1, "Virgin Islands - US (+1)");
        $this->countryCodeItems[] = new CountryPhoneCode( "WF", 681, "Wallis &amp; Futuna (+681)");
        $this->countryCodeItems[] = new CountryPhoneCode( "YE", 969, "Yemen (North)(+969)");
        $this->countryCodeItems[] = new CountryPhoneCode( "YE", 967, "Yemen (South)(+967)");
        $this->countryCodeItems[] = new CountryPhoneCode( "ZM", 260, "Zambia (+260)");
        $this->countryCodeItems[] = new CountryPhoneCode( "ZW", 263, "Zimbabwe (+263)");
    }
}