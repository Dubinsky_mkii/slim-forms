<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\MaxCharsRule;
use slimsky\forms\Validation\Rules\MinCharsRule;
use slimsky\forms\Entity\CheckboxGroupItem;

class RadioBtnGroupField extends FieldType {
    
    public $groupItems = Array();

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/radiogroupfield.twig') {
        parent::__construct($owner, $title, $templatePath);
    }
    
    public function addItem($title, $value, $description) {
        if (array_key_exists($value, $this->groupItems)) {
            throw new \Exception("duplicate value for one checkbox group :".$value);
        }
        $this->groupItems[$value] = new CheckboxGroupItem($title, $value, $description);
        return $this;
    }
    
    public function setValueFrom($value) {
        $this->value = $value[$this->id];
        if ($this->value) {
            if (array_key_exists($this->value, $this->groupItems)) {
                $this->groupItems[$this->value]->checked = true;
            }
        }
        return $this;
    }
    
    protected function getRenderParams() {
        $params = parent::getRenderParams();
        $params['groupItems'] = $this->groupItems;
        return $params;
    }
    
}