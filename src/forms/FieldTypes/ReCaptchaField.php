<?php

namespace slimsky\forms\FieldTypes;

use Slim\Views\Twig;
use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\RecaptchaRule;
use Symfony\Component\Translation\Translator;

class ReCaptchaField extends FieldType {

    private $recaptchRule;

    private $siteKey;

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $siteKey - siteKey from ReCaptcha
     * @param $secretKey - secret key from ReCaptcha
     * @param string $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $siteKey, $secretKey, $templatePath = 'form/fields/captchafield.twig') {
        parent::__construct($owner, $title, $templatePath);
        $this->siteKey = $siteKey;
        $this->recaptchRule = new RecaptchaRule($this->title, $siteKey, $secretKey);
        $this->addValidator($this->recaptchRule);
    }

    /**
     * @param $value - ignored in this case since recaptcha returns different POST variable format
     * @return $this|void
     */
    public function setValueFrom($value) {
        $request = $this->owner->getRequest();
        $this->value = $request->getParsedBody()['g-recaptcha-response'];
    }

    public function getCaptchaRule() {
        return $this->recaptchRule;
    }
    
    protected function getRenderParams() {
        $params = parent::getRenderParams();
        $params['siteKey'] = $this->siteKey;
        return $params;
    }
}