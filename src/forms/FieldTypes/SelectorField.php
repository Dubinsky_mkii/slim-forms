<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use Slim\Views\Twig;
use slimsky\forms\Entity\SelectorItem;
use slimsky\forms\Form;

class SelectorField extends FieldType {

    protected $items = array();

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/selectfield.twig') {
        parent::__construct($owner, $title, $templatePath);
    }

    protected function getRenderParams() {
        $params = parent::getRenderParams();
        $params['items'] = $this->items;
        return $params;
    }
    
    public function addItem($value, $title) {
        $this->items[] = new SelectorItem($value, $title);
        return $this;
    }
}