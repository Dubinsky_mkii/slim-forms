<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 17:34
 */

namespace slimsky\forms\FieldTypes;

use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\MaxCharsRule;
use slimsky\forms\Validation\Rules\MinCharsRule;

class TextField extends FieldType {

    protected $minCharsRule;

    protected $maxCharsRule;

    protected $isPassword = false;

    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/textfield.twig') {
        parent::__construct($owner, $title, $templatePath);
        $this->minCharsRule = new MinCharsRule($this->title, 0);
        $this->maxCharsRule = new MaxCharsRule($this->title, PHP_INT_MAX);
        $this->addValidator($this->minCharsRule);
        $this->addValidator($this->maxCharsRule);
    }

    public function setMinChars($count, $errorMessage = null) {
        $this->minCharsRule->setMinValue($count);
        if ($errorMessage) {
            $this->minCharsRule->message = $errorMessage;
        }
        return $this;
    }

    public function setMaxChars($count, $errorMessage = null) {
        $this->maxCharsRule->setMaxValue($count);
        if ($errorMessage) {
            $this->maxCharsRule->message = $errorMessage;
        }
        return $this;
    }

    public function setIsPassword($isPassword) {
        $this->isPassword = $isPassword;
        return $this;
    }

    public function isPassword() {
        return $this->isPassword;
    }

    public function getMinCharsRule() {
        return $this->minCharsRule;
    }

    public function getMaxCharsRule() {
        return $this->maxCharsRule;
    }
}