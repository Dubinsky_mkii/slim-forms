<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.27.1
 * Time: 16:41
 */

namespace slimsky\forms\FieldTypes;

use Respect\Validation\Validator as v;
use slimsky\forms\Form;
use slimsky\forms\Validation\Rules\UrlRule;

class UrlField extends FieldType {

    private $urlValidator;
    /**
     * @param $owner Form - slimsky form class this field will be used in.
     * @param $title - Title of the field, will be rendered in template.
     * @param $templatePath - template path, relative to the project root directory.
     */
    public function __construct($owner, $title, $templatePath = 'form/fields/textfield.twig') {
        parent::__construct($owner, $title, $templatePath);
        $this->urlValidator = new UrlRule($this->title);
        $this->addValidator($this->urlValidator);
    }

    public function getUrlRule() {
        return $this->urlValidator;
    }
}