<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.24.1
 * Time: 16:13
 */

namespace slimsky\forms;

use Slim\Http\Request;
use Slim\Views\Twig;
use slimsky\forms\FieldTypes\CsrfField;
use slimsky\forms\FieldTypes\FieldType;
use Symfony\Component\Translation\Translator;

class Form {

    /**
     * @var FieldType[]
     */
    protected $fields = array();

    private $id;

    private $request;

    private $templatePath;

    private $action;

    protected $csrfField;

    /**
     * @param $request Request
     * @param $formId
     * @param string $templatePath - form template path, relative to the project root directory.
     */
    public function __construct($request, $formId, $templatePath = 'form/form.twig') {
        $this->id = $formId;
        $this->request = $request;
        $this->action = $request->getUri()->getPath();
        if (!$templatePath) {
            $this->templatePath = 'form/form.twig';
        } else {
            $this->templatePath = $templatePath;
        }

        $this->csrfField = new CsrfField($this, "Save URL", 'form/fields/csrf.twig');
        $this->add(
            $this->csrfField
                ->setId("csrf"));

    }

    /**
     * @param $field FieldType - field to add to form
     */
    protected function add($field) {
        array_push($this->fields, $field);
    }

    public function isSubmitted() {
        $allPostPutVars = $this->request->getParsedBody();
        if ($this->request->isPost() && array_key_exists($this->id, $allPostPutVars))
            return true;
        return false;
    }

    public function isValid() {
        foreach($this->fields as $field) {
            if (!$field->isValid()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $view Twig - view engine to generate view from
     * @param $translator Translator
     * @return string - generated html string of form
     * @throws \Exception in case of duplicate fields throws an exception
     */
    public function getView($view, $translator = null) {
        return $view->fetch($this->templatePath, $this->getRenderObject($view, $translator));
    }

    /**
     * Prepares object for rendering, may be used for custom form rendering in page template
     */
    public function getRenderObject($view, $translator = null) {
        $args = Array();
        $args['fields'] = array();
        $args['rows'] = Array();

        $validate = $this->isSubmitted();
        $args['validate'] = $validate;
        $args['form'] = $this;
        foreach($this->fields as $field) {
            if (array_key_exists($field->getId(), $args['fields'])) {
                throw new \Exception('duplicate field id '.$field->getId().' in form');
            }
            $args['fields'][$field->getId()] = $field;
            $args['rows'][$field->getId()] =  $field->getView($view, $translator, $validate);
        }
        return $args;
    }

    protected function fillValues() {
        if ($this->request->isPost()) {
            $allPostPutVars = $this->request->getParsedBody();
            if( array_key_exists($this->id, $allPostPutVars)) {
                foreach ($this->fields as $field) {
                    $field->setValueFrom($allPostPutVars[$this->id]);
                }
            }
        }
    }

    public function getId() {
        return $this->id;
    }

    public function getAction() {
        return $this->action;
    }

    /**
     * @return Request
     */
    public function getRequest() {
        return $this->request;
    }
}