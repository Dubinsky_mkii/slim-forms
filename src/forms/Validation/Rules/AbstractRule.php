<?php

namespace slimsky\forms\Validation\Rules;

use Symfony\Component\Translation\Translator;

class AbstractRule {

    protected $name;

    protected $enabled = true;

    public $message = 'error.validation.generic_message';

    public function __construct($name) { $this->name = $name; }

    public function getName() { return $this->name; }

    public function validate($input) { return false; }

    public function setEnabled($enabled) {
        $this->enabled = $enabled;
    }

    /**
     * @param $input
     * @param $translator Translator
     */
    public function getErrors($input, $translator) { return null;}

    /**
     * Fallback message in case translator wasn't provided
     * @param $params
     * @return string
     */
    public function formatMessage($params = []) {
        return vsprintf($this->message, $params);
    }
}