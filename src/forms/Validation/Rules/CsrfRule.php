<?php

namespace slimsky\forms\Validation\Rules;

use slimsky\forms\FieldTypes\CsrfField;

class CsrfRule extends AbstractRule{

    /** @var  CsrfField */
    private $field;

    public function __construct($name, $field) {
        parent::__construct($name);
        $this->field = $field;
        $this->message = 'error.validation.csrf_mismatch';
    }

    public function validate($input) {
        if (!$this->enabled) {
            return true;
        }
        if (isset($_SESSION[$this->field->getFieldName()])) {
            return $input == $_SESSION[$this->field->getFieldName()];
        } else {
            return false;
        }
    }
}