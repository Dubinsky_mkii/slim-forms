<?php

namespace slimsky\forms\Validation\Rules;

class EmailRule extends AbstractRule{

    public function __construct($name) {
        parent::__construct($name);
        $this->message = 'error.validation.email';
    }

    public function validate($input) {
        if (!$this->enabled) {
            return true;
        }
        return is_string($input) && filter_var($input, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name);
            if ($translator != null) {
                return $translator->trans($params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }
}