<?php

namespace slimsky\forms\Validation\Rules;

class MaxCharsRule extends AbstractRule{
    
    private $maxVal;

    public function __construct($name, $max) {
        parent::__construct($name);
        $this->message = 'error.validation.max_chars';
        $this->maxVal = $max;
    }

    public function setMaxValue($min) {
        if (!$this->enabled) {
            return true;
        }
        $this->maxVal = $min;
    }

    public function validate($input) {
        return strlen($input) <= $this->maxVal;
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name, '%max%' => $this->maxVal);
            if ($translator != null) {
                return $translator->trans($params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }
}