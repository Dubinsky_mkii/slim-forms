<?php

namespace slimsky\forms\Validation\Rules;

class MinCharsRule extends AbstractRule{
    
    private $minVal;

    public function __construct($name, $min) {
        parent::__construct($name);
        $this->message = 'error.validation.min_chars';
        $this->minVal = $min;
    }

    public function validate($input) {
        if (!$this->enabled) {
            return true;
        }
        return strlen($input) >= $this->minVal;
    }

    public function setMinValue($min) {
        $this->minVal = $min;
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name, '%min%' => $this->minVal);
            if ($translator != null) {
                return $translator->trans($params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }
}