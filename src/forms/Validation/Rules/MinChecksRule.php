<?php

namespace slimsky\forms\Validation\Rules;

class MinChecksRule extends AbstractRule{
    
    private $minChecks;

    public function __construct($name, $min) {
        parent::__construct($name);
        $this->message = 'error.validation.min_checks';
        $this->minChecks = $min;
    }

    public function validate($input) {
        if (!$this->enabled) {
            return true;
        }
        return sizeof($input) >= $this->minChecks;
    }

    public function setMinChecks($min) {
        $this->minChecks = $min;
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name, '%min%' => $this->minChecks);
            if ($translator != null) {
                return $translator->trans($params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }
}