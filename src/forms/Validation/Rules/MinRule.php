<?php

namespace slimsky\forms\Validation\Rules;

class MinRule extends AbstractRule{

    private $minVal;

    public function __construct($name, $min = PHP_INT_MAX) {
        parent::__construct($name);
        $this->message = 'error.validation.min';
        $this->minVal = $min;
    }

    public function setMinValue($min) {
        $this->minVal = $min;
    }

    public function validate($input) {
        if (!$this->enabled) {
            return true;
        }
        return $input >= $this->minVal;
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name, '%min%' => $this->minVal);
            if ($translator != null) {
                return $translator->trans($params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }
}