<?php

namespace slimsky\forms\Validation\Rules;

use stdClass;
use Symfony\Component\Translation\Translator;

class NotBlankRule extends AbstractRule {

    public function __construct($name, $message = null) {
        parent::__construct($name);
        if ($message) {
            $this->message = $message;
        } else {
            $this->message = 'error.validation.not_blank';
        }
    }

    public function validate($input) {
        if (!$this->enabled) {
            return true;
        }
        if (is_numeric($input)) {
            return $input != 0;
        }

        if (is_string($input)) {
            $input = trim($input);
        }

        if ($input instanceof stdClass) {
            $input = (array) $input;
        }

        if (is_array($input)) {
            $input = array_filter($input, __METHOD__);
        }

        return !empty($input);
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name);
            if ($translator != null) {
                return $translator->trans($this->message, $params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }
}