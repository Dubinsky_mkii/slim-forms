<?php

namespace slimsky\forms\Validation\Rules;

use slimsky\forms\FieldTypes\PhoneField;

class PhoneRule extends AbstractRule{

    /** @var  PhoneField */
    private $field;

    public function __construct($name, $field) {
        parent::__construct($name);
        $this->message = 'error.validation.phone';
        $this->field = $field;
    }

    public function validate($input) {
        if (!$this->enabled) {
            return true;
        }
        if (!$this->notBlank($this->field->getPhoneCode())) {
            return false;
        }
        if (!$this->notBlank($this->field->getPhoneNumber())) {
            return false;
        }
        return true;
    }

    private function notBlank($input) {
        if (is_numeric($input)) {
            return $input != 0;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name);
            if ($translator != null) {
                return $translator->trans($params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }
}