<?php

namespace slimsky\forms\Validation\Rules;

use GuzzleHttp\Client;

class RecaptchaRule extends AbstractRule{

    private $checked = false;

    private $valid = false;

    private $siteKey;

    private $secretKey;

    public function __construct($name, $siteKey, $secretKey) {
        parent::__construct($name);
        $this->siteKey = $siteKey;
        $this->secretKey = $secretKey;
        $this->message = 'error.validation.captcha_failed';
    }

    public function validate($input) {
        if (!$this->enabled) {
            return true;
        }
        if (!$this->checked) {
            $this->checkReCaptchaValid($input);
        }
        return $this->valid;
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name);
            if ($translator != null) {
                return $translator->trans($params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }

    private function checkReCaptchaValid($input) {
        $this->checked = true;
        $client = new Client();
        try {
            $res = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => [
                    'secret' => $this->secretKey,
                    'response' => $input
                ]
            ]);
            $response = json_decode($res->getBody());
            $this->valid = $response->success;
        } catch (\Exception $e) {
            error_log("CaptchaError: CODE ".$e->getCode()." FILE ".$e->getFile()." LINE ".$e->getLine()." MESSAGE ".$e->getMessage());
            $this->valid = false;
        }
    }
}