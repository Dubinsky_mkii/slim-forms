<?php

namespace slimsky\forms\Validation\Rules;

use slimsky\forms\FieldTypes\FieldType;
use Symfony\Component\Translation\Translator;

class SameValueRule extends AbstractRule {

    /** @var  FieldType */
    private $checkField;

    /**
     * @param $name
     * @param $checkField
     * @param null $message
     */
    public function __construct($name, $checkField, $message = null) {
        parent::__construct($name);
        $this->checkField = $checkField;
        if ($message) {
            $this->message = $message;
        } else {
            $this->message = 'error.validation.not_same';
        }
    }

    public function validate($input) {
        if (!$this->enabled) {
            return true;
        }
        return $input == $this->checkField->getValue();
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name);
            if ($translator != null) {
                return $translator->trans($this->message, $params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }
}