<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 17.27.1
 * Time: 16:34
 */

namespace slimsky\forms\Validation\Rules;

use dubinsky\uritils\Uritils;
use Mso\IdnaConvert;

class UrlRule extends AbstractRule {


    public function __construct($name) {
        parent::__construct($name);
        $this->message = 'error.validation.url';
    }

    public function validate($url)
    {
        if (!$this->enabled) {
            return true;
        }
        // Do something here with the $input and return a boolean value
        if (isset($url) && strlen($url) > 0)
        {
            $uritils = new Uritils();
            $url = $uritils->canonizeUrl($url);
            $url = $uritils->getAsciiUrl($url);
            $url = filter_var($url, FILTER_SANITIZE_URL);
            if (filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED) === false ||
                strpos($url, '.') === false)
            {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getErrors($input, $translator = null) {
        if (!$this->validate($input)) {
            $params = array('%name%' => $this->name);
            if ($translator != null) {
                return $translator->trans($this->message, $params);
            } else {
                return $this->formatMessage($params);
            }
        }
        return null;
    }
}